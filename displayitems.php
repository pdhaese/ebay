<?php
//select.php
require 'database.php';
//header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$output = array();

class anItem {
          // Creating some properties (variables tied to an object)
          public $thistitle;
          public $thisdescription;
          public $thisprice;
          public $thisseller;
          public $numberofbids;
          public $thisitemid;
          public $category;

          // Assigning the values
          public function __construct($title, $description, $price, $seller, $bids, $itemid, $category) {
            $this->thistitle = $title;
            $this->thisdescription = $description;
            $this->thisprice = $price;
            $this->thisseller = $seller;
            $this->numberofbids = $bids;
            $this->thisitemid = $itemid;
            $this->category = $category;
          }
        }

//need to add who it's sold by
$stmt = $mysqli->prepare("SELECT title, description, price, userid, itemid, category FROM items");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();

$stmt->bind_result($title, $des,$pr,$uid, $iid, $cat);

while($stmt->fetch()){

    $it = new anItem(htmlentities($title), htmlentities($des), htmlentities($pr), $uid, 0, $iid, $cat);
    array_push($output, $it);
}

$stmt->close();

for($i = 0; $i < sizeof($output); $i++){
  $query = $mysqli->prepare("SELECT username FROM users WHERE id=?");
  if(!$query){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
  }
  $findme = $output[$i]->thisseller;
  $query->bind_param('s', $findme);

  $query->execute();

  $query->bind_result($result);

  $query->fetch();
  $output[$i]->thisseller = htmlentities($result);
  $query->close();

  $getbidsquery = $mysqli->prepare("SELECT price FROM bids WHERE itemid =?");
  if(!$getbidsquery){
  	printf("Query Prep Failed: %s\n", $mysqli->error);
  	exit;
  }

  $getbidsquery->bind_param('s', $output[$i]->thisitemid);

  $getbidsquery->execute();

  $getbidsquery->bind_result($ppppp);

  while($getbidsquery->fetch()){
    $output[$i]->numberofbids ++;
  }
  $getbidsquery->close();
}




echo json_encode(array(
  "success"=> true,
  "output" => $output
));

?>
