<?php
//select.php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$myuser = $data->user;
$output = array();

class anItem {
          // Creating some properties (variables tied to an object)
          public $thismessage;
          public $thissender;
          public $thisuser;

          // Assigning the values
          public function __construct($message, $sender, $user) {
            $this->thismessage = $message;
            $this->thissender = $sender;
            $this->thisuser = $user;
          }
        }

//need to add who it's sold by
$stmt = $mysqli->prepare("SELECT message,sender,user FROM messages WHERE user=?");
$stmt->bind_param('s', $myuser);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();

$stmt->bind_result($resultmessage, $resultsender,$resultuser);

while($stmt->fetch()){

    $it = new anItem(htmlentities($resultmessage), htmlentities($resultsender),htmlentities($resultuser));
    array_push($output, $it);
}

$stmt->close();

// for($i = 0; $i < sizeof($output); $i++){
//   $query = $mysqli->prepare("SELECT username FROM users WHERE id=?");
//   if(!$query){
//   printf("Query Prep Failed: %s\n", $mysqli->error);
//   exit;
//   }
//   $findme = $output[$i]->thisseller;
//   $query->bind_param('s', $findme);
//
//   $query->execute();
//
//   $query->bind_result($result);
//
//   $query->fetch();
//   $output[$i]->thisseller = $result;
//   $query->close();
// }




echo json_encode(array(
  "success"=> true,
  "output" => $output
));

?>
