<?php
//select.php
require 'database.php';
//header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$seller = $data->currentuser;
$bidid = $data->bidid;

//get the item title, bid price, bidder name
$selleremailquery = $mysqli->prepare("SELECT email FROM users WHERE username=?");
if(!$selleremailquery){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$selleremailquery->bind_param('s', $seller);

$selleremailquery->execute();

$selleremailquery->bind_result($email);

$selleremailquery->fetch();
$selleremailquery->close();


//get bid price and itemid, userid
$bidquery = $mysqli->prepare("SELECT price, itemid, userid FROM bids WHERE bidid=?");
if(!$bidquery){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$bidquery->bind_param('s', $bidid);

$bidquery->execute();

$bidquery->bind_result($bidprice, $itemid, $bidderid);

$bidquery->fetch();
$bidquery->close();


//get item title
$bidquery = $mysqli->prepare("SELECT title FROM items WHERE itemid=?");
if(!$bidquery){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$bidquery->bind_param('s', $itemid);

$bidquery->execute();

$bidquery->bind_result($itemtitle);

$bidquery->fetch();
$bidquery->close();

$useridq = $mysqli->prepare("SELECT username FROM users WHERE id=?");
if(!$useridq){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$useridq->bind_param('s', $bidderid);

$useridq->execute();

$useridq->bind_result($bidderusername);

$useridq->fetch();
$useridq->close();

//$bidprice = string($bidprice);
$message = $seller . " has accepted your bid for " . $itemtitle . " for $" . $bidprice . ". Contact them at " . $email;
//$message = "hello";
$sender = "eBay Seller Notification";

$query = $mysqli->prepare("INSERT into messages (message, sender, user) values (?, ?, ?)");
$query->bind_param('sss', $message, $sender, $bidderusername);
	// if(!$stmt){
	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
	// 	exit;
	// }
$query->execute();
$query->close();

//if we accepted the bid, we can delete all bids on that item and the item, because it's no longer for sale
$deletebidquery = $mysqli->prepare("DELETE FROM bids WHERE itemid=?");


$deletebidquery->bind_param('s', $itemid);

$deletebidquery->execute();
$deletebidquery->close();


$deletequery = $mysqli->prepare("DELETE FROM items WHERE itemid=?");


$deletequery->bind_param('s', $itemid);

$deletequery->execute();
$deletequery->close();



echo json_encode(array(
  "success"=> true,
  "bidid" => $bidid,
  "message" => htmlentities($message),
  "itemid" => $itemid,
  "bidderusername" => $bidderusername,
  "biditemtitle" => $itemtitle
));

?>
