<?php
require 'database.php';
session_start();
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$itemid = $data->itemid;
$username = $data->currentuser;

//Pass XSRF token
$headerToken = $_SERVER['HTTP_X_XSRF_TOKEN'];
   if ($headerToken != $_SESSION['XSRF-TOKEN']){
     die("Request forgery detected");
   }

$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $username);

$stmt->execute();

$stmt->bind_result($userid);

$stmt->fetch();
$stmt->close();

$prep = $mysqli->prepare("SELECT userid FROM items WHERE itemid=?");
$prep->bind_param('s', $itemid);
if(!$prep){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$prep->bind_param('s', $itemid);
$prep->execute();

$prep->bind_result($itemuserid);

$prep->fetch();
$prep->close();

if($itemuserid == $userid){
  $querydeletebids = $mysqli->prepare("DELETE FROM bids WHERE itemid=?");
  $querydeletebids->bind_param('s',$itemid);
  	// if(!$stmt){
  	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
  	// 	exit;
  	// }
  $querydeletebids->execute();
  $querydeletebids->close();
  $query = $mysqli->prepare("DELETE FROM items WHERE itemid=?");
  $query->bind_param('s',$itemid);
  	// if(!$stmt){
  	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
  	// 	exit;
  	// }
  $query->execute();
  $query->close();
    echo json_encode(array(
      "success" => true,
      "itemid" =>$itemid,
      "username" => $userid,
      "itemusername"=>$itemuserid
    ));
    exit;
}



?>
