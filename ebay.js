
var app = angular.module('myApp', []);


//app.controller('myCtrl', ['$scope', 'fileUpload', '$http', function($scope, $http, fileUpload) {
app.controller('myCtrl', function($scope, $http) {
  //  $scope.count = 0;
	var currentuser = "";
	var allitems = [];

	$scope.myVar = false;
	$scope.afterLogin = false;
	$scope.otherafterLogin = false;
	$scope.anotherafterLogin = false;
	$scope.toggle = function() {
        $scope.myVar = !$scope.myVar;
    };


		//Display the items the current user is selling
		showMyItems = function(){
			$http.post(
						"showmyitems.php",
						{'currentuser':currentuser}
					).then(function(response){
					if(response.data.success){
						console.log("show my items success!");
						$scope.myitems = response.data.outputitems;
						console.log("my stuff");
						console.log(response.data.outputitems);

					}

				});
		}

		//Logout function
		$scope.logoutClick = function(){
			currentuser = "";
			$scope.afterLogin = false;
			$scope.otherafterLogin = false;
			$scope.anotherafterLogin = false;
			$scope.currentuser = "";
			alert("You logged out!");
		}
		$scope.acceptBid = function(item){
			var bididObject= document.getElementById(item.bidid);
			console.log("bid id object: " + bididObject);
			var bidid = bididObject.textContent;
			console.log("bid id: " + bidid);
			$http.post(
						"acceptbid.php",
						{'currentuser':currentuser, 'bidid': bidid}
					).then(function(response){
					if(response.data.success){
						alert("You have accepted " + response.data.bidderusername + "'s bid on your item: " + response.data.biditemtitle);
						console.log("accept bid success!");
						console.log("bidid: " + response.data.bidid);
						console.log("message: " + response.data.message);
						console.log("itemid: " + response.data.message);
						showMyItems();
						displayItems();
						showMyBids();
						//$scope.myitems = response.data.outputitems;


					}

				});
		}
		//Saves the user edits to the database
		$scope.editClick = function(){

			console.log("token: " + $scope.token);

			$http.post(
						"edititem.php",
						{'itemtitle':$scope.edititemtitle, 'description':$scope.editdescription, 'price':$scope.editprice, 'itemid':$scope.editid, 'currentuser':currentuser}
					).then(function(response){
						alert("Edit was made");
						showMyItems();
						displayItems();
						showMyBids();
				});
			}

			//Saves the user edits to the database
			$scope.deleteClick = function(){
				console.log("deleteid: " + $scope.deleteid);
					console.log("current user: " + currentuser);
					
				$http.post(
							"deleteitem.php",
							{'itemid':$scope.deleteid, 'currentuser':currentuser}
						).then(function(response){
							alert("Item was removed");
							console.log("item id: " + response.data.itemid);
							console.log("user id: " + response.data.username);
							console.log("item user id: " + response.data.itemusername);

							showMyItems();
							displayItems();
							showMyBids();
					});
				}
				//parsed the itemtitles to see if they are as long as the search and then compare them to see if they match the search given. Returns all when nothing is input
				$scope.searchClick = function(){
					var searchresults = [];
					var mysearch = $scope.searchbar;
					var searchitems = allitems;
					if(mysearch.length != 0){
						for(a=0;a<searchitems.length;a++){
							var testtitle = searchitems[a].thistitle;
							if(testtitle.length >= mysearch.length){
								var parsedtitle = testtitle.substring(0,mysearch.length);
								if(parsedtitle.toLowerCase() == mysearch.toLowerCase()){
									searchresults.push(searchitems[a]);
								}
							}
						}
						$scope.items = searchresults;

					} else {
						$scope.items = searchitems;
					}
				}

				//Search items by category
				$scope.categorySearchClick = function(){
					var searchresults = [];
					var categorysearch = $scope.categorysearch;
					var searchitems = allitems;

					if(categorysearch == "none"){
						$scope.items = searchitems;
						console.log("no search filter");
					}
					else{
						for(a=0;a<searchitems.length;a++){
							var foundcategory = searchitems[a].category;
								if(foundcategory == categorysearch){
									console.log("found category");
									searchresults.push(searchitems[a]);
								}
							}

						$scope.items = searchresults;

					}

					}

					//sends a message to another user
				$scope.messageClick = function(){
					$http.post(
								"message.php",
								{'message':$scope.message, 'sender':currentuser, 'user':$scope.messageuser}
							).then(function(response){
								alert("message was sent");
						});
					}
					//saves a rating for another user
					$scope.rateClick = function(){
						$http.post(
									"rate.php",
									{'rating':$scope.rating, 'rater':currentuser, 'user':$scope.rateuser}
								).then(function(response){
									alert("You submitted a rating");
							});
						}

		//Display bids on the current user's items that they are selling
		showMyBids = function(){
			$http.post(
						"getbidders.php",{'thisuser':currentuser}
					).then(function(data){
					if(data.data.success){
						console.log("show my bids success!");
						console.log("outputbids");
						console.log("showmybids" + data.data.output);
						$scope.mybids = data.data.output;

					}
					else{
						console.log("get bidders failed");
					}

				});
		}
		//queries the database for a users average rating
		$scope.checkClick = function(){
			$http.post(
						"checkrating.php",
						{'user':$scope.checkuser}
					).then(function(response){
						alert("User rating: " + response.data.output);
				});
			}

			//displays messages to the current user, including the message that says a seller accepted their bid
			displayMessages = function(){
				$http.post("displaymessages.php",
				{'user':currentuser}
				).then(function (response) {
						if(response.data.success){
							console.log("success!");
							$scope.messages = response.data.output;
							console.log(response.data.output);
						}
					});
				}

	//bid on an item
	$scope.bid = function(item) {
		var usetitle = item.thistitle;
		console.log("use title: " + usetitle);

		var bidid= document.getElementById(usetitle);
		var bidamount = bidid.value;
		console.log("bidid: " + bidid);
		console.log("bidamount" + bidamount);


		$http.post(
					"bid.php",
					{'itemtitle':usetitle,'bid':bidamount,'currentuser':currentuser}
				).then(function(data){
									//alert(data.data.);
									//console.log(data);
					console.log(item.thistitle);
									if(data.data.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
										alert("You have bid on " + data.data.item);
										console.log("bid on item");
										console.log(data.data.userid);
										console.log(data.data.username);
										console.log(data.data.itemid);
										console.log(data.data.bidprice);
										showMyBids();
										showMyItems();
										displayItems();

									}
									else{
										alert("Your bid did not go through.");
									}

						});

	}


//
	displayItems = function(){
		$http.get("displayitems.php")
			.then(function (response) {
				if(response.data.success){
					console.log("success!");
					$scope.items = response.data.output;
					allitems = $scope.items;
					console.log(response.data.output);

				}
			});
	}

displayItems();

	//post an item you would like to sell
	$scope.postItemClick = function() {
		$http.post(
					"postitem.php",
					{'itemtitle':$scope.itemtitle, 'description':$scope.description, 'price':$scope.price, 'currentuser':currentuser,'category': $scope.category}
				).then(function(data){
									//alert(data.data.);
									//console.log(data);

									if(data.data.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
										alert("Your item has been posted for sale!");
										console.log("posted item");
										console.log(data.data);
										displayItems();
										showMyItems();
									}
									else{
										alert("Your item was not posted.");
									}

									//$scope.displayData();
						 });
	}

//Display the current user's name at the top of the page
	displayUser = function(user){
		console.log("in display user");
		$scope.currentuser = "Hello "+ user;
		currentuser = user;
		displayMessages();
		console.log("current user:")
		console.log(currentuser);
	}

	//login a user. Passwords not required.
    $scope.loginClick = function() {
			$http.post(
				"login.php",
				{'logusername':$scope.logusername}
			).then(function(data){
				console.log($scope.logusername);
				//console.log($scope.logpassword);
				$scope.logusername = null;
				$scope.logpassword = null;
				if(data.data.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
					if(currentuser !=""){
						alert("You must log out first!");
					}
					else{
						alert("You've been Logged In!");
						currentuser = data.data.username;
						showMyItems();
						showMyBids();
						displayUser(data.data.username);
						$scope.afterLogin = true;
						$scope.otherafterLogin = true;
						$scope.anotherafterLogin = true;
					}

				}
				else{
					alert("You were not logged in...  "+data.data.message);
					console.log("guess: " + data.data.pwdguess);
					console.log("hash after trim " +data.data.pwdhash);
					console.log("hash before trim " +data.data.pwdbeforetrim);
					console.log("hard hash " +data.data.hardhash);
					console.log("length pass hash login " +data.data.hashlength);
					console.log("length pwd guess " +data.data.guesslength);

					console.log(data.data.username);
					// displayUser(data.data.username);
					// //delete this later
					// showMyItems();
					// showMyBids();
					// 	$scope.afterLogin = true;
					// 	$scope.otherafterLogin = true;
				}
			});
    }


		//register a user with a username and email
		$scope.registerClick = function() {
			$http.post(
						"register.php",
						{'registerusername':$scope.registerusername, 'registeremail': $scope.registeremail}
					).then(function(data){
										//alert(data.data.);
										//console.log(data);
										$scope.registerusername = null;
										$scope.registerpassword = null;
										if(data.data.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
											alert("You've been Registered!");
											console.log("registered");

											console.log("hash: " + data.data.passhash);
											console.log("hash length: " + data.data.lengthpasshash)
										}
										else{
											alert("You were not registered...  "+data.data.message);
										}

										//$scope.displayData();
							 });
    }


});
