<?php
require 'database.php';

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$message = $data->message;
$sender = $data->sender;
$user = $data->user;


$query = $mysqli->prepare("INSERT into messages (message, sender, user) values (?, ?, ?)");
$query->bind_param('sss', $message, $sender, $user);
	// if(!$stmt){
	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
	// 	exit;
	// }
$query->execute();
$query->close();
  echo json_encode(array(
    "success" => true
  ));
  exit;


?>
