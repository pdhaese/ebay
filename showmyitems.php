<?php
//select.php
require 'database.php';
//header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$outputitems = array();
$outputbids = array();
$username = $data->currentuser;

//make item with attributes title and id
//push onto array
//return the array of items to the js side

                class SaleItem {
           // Creating some properties (variables tied to an object)
           public $saletitle;
           public $saleitemid;


           // Assigning the values
           public function __construct($saletitle, $saleitemid) {
             $this->saletitle = $saletitle;
             $this->saleitemid = $saleitemid;

           }
         }

        //   array_push($outputbids, $bi);

$query = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$query){
printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
        }
$query->bind_param('s', $username);

$query->execute();

$query->bind_result($userid);

$query->fetch();
$query->close();
//
// //
// //need to add who it's sold by
$stmt = $mysqli->prepare("SELECT title, itemid FROM items WHERE userid=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('s', $userid);
$stmt->execute();

$stmt->bind_result($title, $itid);

while($stmt->fetch()){
  $mysaleitem = new SaleItem(htmlentities($title), htmlentities($itid));
    array_push($outputitems, $mysaleitem);
}

$stmt->close();

echo json_encode(array(
  "success"=> true,
  "outputitems" => $outputitems
));

?>
