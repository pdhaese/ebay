<?php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$username = $data->logusername;
// $pwd_guess = $data->logpassword;
// $pwd_guess=trim($pwd_guess);
$stmt = $mysqli->prepare("SELECT username FROM users WHERE username=?");

$stmt->bind_param('s', $username);
$stmt->execute();

//Bind the results
$stmt->bind_result($u);
$stmt->fetch();

if($u != null){
  echo json_encode(array(
  		"success" => true,
      "username" => htmlentities($username)
  	));
    ini_set("session.cookie_httponly", 1);
    session_start();
  	$_SESSION['username'] = $username;
  	//$_SESSION['token'] = substr(md5(rand()), 0, 10);
  //  $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
  $_SESSION['XSRF-TOKEN'] = bin2hex(openssl_random_pseudo_bytes(32));
    setcookie('XSRF-TOKEN', $_SESSION['XSRF-TOKEN']);
}

else {
  $stmt->close();
  echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
    // "pwdguess"=> $pwd_guess,
    // "pwdbeforetrim" => $pwd_hash,
    // "pwdhash" =>$pwd_trim,
    // "username" => $username,
    // "guesslength" => $guesslength,
    // "hashlength" => $hashlength
	));

}

?>
