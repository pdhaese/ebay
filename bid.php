<?php
require 'database.php';

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$title = $data->itemtitle;
$username = $data->currentuser;
$bid = $data->bid;

$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $username);

$stmt->execute();

$stmt->bind_result($userid);

$stmt->fetch();
$stmt->close();

$qs = $mysqli->prepare("SELECT itemid FROM items WHERE title=?");
if(!$qs){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$qs->bind_param('s', $title);

$qs->execute();

$qs->bind_result($itemid);

$qs->fetch();
$qs->close();


$query = $mysqli->prepare("INSERT into bids (price, itemid, userid) values (?, ?, ?)");
	// if(!$stmt){
	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
	// 	exit;
	// }

$query->bind_param('sss', $bid, $itemid, $userid);
$query->execute();
$query->close();
  echo json_encode(array(
    "success" => true,
    "item" => $title,
		"userid" => $userid,
		"username" => $username,
    "itemid" => $itemid,
    "bidprice" => htmlentities($bid)


  ));


?>
