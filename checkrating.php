<?php
//select.php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$myuser = $data->user;
$mycount = 0;
$myvalue = 0;

//need to add who it's sold by
$stmt = $mysqli->prepare("SELECT rating FROM ratings WHERE user=?");
$stmt->bind_param('s', $myuser);
// if(!$stmt){
// 	printf("Query Prep Failed: %s\n", $mysqli->error);
// 	exit;
// }

$stmt->execute();

$stmt->bind_result($resultrating);

while($stmt->fetch()){
  $myvalue = $myvalue + $resultrating;
  $mycount = $mycount + 1;
}

$stmt->close();

$endrating = 0;
if($mycount>0){
  $endrating = $myvalue/$mycount;
}

echo json_encode(array(
  "success"=> true,
  "output" => $endrating
));

?>
