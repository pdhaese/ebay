<?php
require 'database.php';
session_start();
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$title = $data->itemtitle;
$description = $data->description;
$price = $data->price;
$itemid = $data->itemid;
$username = $data->currentuser;


$headerToken = $_SERVER['HTTP_X_XSRF_TOKEN'];

   if ($headerToken != $_SESSION['XSRF-TOKEN']){
     die("Request forgery detected");
   }

// if(!hash_equals($_SESSION['token'], $token)){
// 	die("Request forgery detected");
// }

$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
$stmt->bind_param('s', $username);
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->execute();

$stmt->bind_result($userid);

$stmt->fetch();
$stmt->close();

$prep = $mysqli->prepare("SELECT userid FROM items WHERE itemid=?");
$prep->bind_param('s', $itemid);
if(!$prep){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}


$prep->execute();

$prep->bind_result($itemuserid);

$prep->fetch();
$prep->close();

if($itemuserid == $userid){
  $query = $mysqli->prepare("UPDATE items SET title = ?,description = ?, price = ? WHERE itemid = ?");
  $query->bind_param('ssss', $title, $description, $price, $itemid);
  	// if(!$stmt){
  	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
  	// 	exit;
  	// }
  $query->execute();
  $query->close();
    echo json_encode(array(
      "success" => true
    ));
    exit;
}



?>
