<?php
require 'database.php';

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$title = $data->itemtitle;
$description = $data->description;
$price = $data->price;
$username = $data->currentuser;
if (isset($data->category)) {
  $category = $data->category;
}
else{
  $category = null;
}


$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $username);

$stmt->execute();

$stmt->bind_result($userid);

$stmt->fetch();
$stmt->close();


$query = $mysqli->prepare("INSERT into items (title, description, price, userid, category) values (?, ?, ?, ?, ?)");
	// if(!$stmt){
	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
	// 	exit;
	// }

$query->bind_param('sssss', $title, $description, $price, $userid, $category);
$query->execute();
$query->close();
  echo json_encode(array(
    "success" => true,
		"user id" => $userid,
		"user name" => $username
  ));
  exit;


?>
