<?php
//select.php
require 'database.php';
//header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
$data = json_decode(file_get_contents("php://input"));
$thisuser = $data->thisuser;

//if we have this user, we should get all the items that attach to their name
//then get all the bids attached to that item and display it on the webpage
$output = array();
$biditems = array();

//bid object--contains a bid price, itemtitle, bidder, and bidid
class bid {
          // Creating some properties (variables tied to an object)
          public $price;
          public $itemtitle;
          public $bidder;
          public $bidid;


          // Assigning the values
          public function __construct($price, $itemtitle, $bidder, $bidid) {
            $this->price = $price;
            $this->itemtitle = $itemtitle;
            $this->bidder = $bidder;
            $this->bidid=$bidid;

          }
        }

//the actual item--just a title and itemid
class bidItem {
          // Creating some properties (variables tied to an object)
          public $thistitle;
          public $thisid;

          // Assigning the values
          public function __construct($thistitle, $thisid) {
            $this->thistitle = $thistitle;
            $this->thisid = $thisid;

          }
        }


$useridq = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$useridq){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$useridq->bind_param('s', $thisuser);

$useridq->execute();

$useridq->bind_result($userid);

$useridq->fetch();
$useridq->close();

//find all items attached to this user
$qs = $mysqli->prepare("SELECT title, itemid FROM items where userid =?");
if(!$qs){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$qs->bind_param('s', $userid);
$qs->execute();

$qs->bind_result($title, $itemid);

while($qs->fetch()){

    $bidit = new bidItem(htmlentities($title), $itemid);
    array_push($biditems, $bidit);
}

$qs->close();
//this gives us all items that have been bid on


//now we make an array of bid objects, each containing the price, itemid, and bidderid
for($j = 0; $j < sizeof($biditems); $j++){
  $stmt = $mysqli->prepare("SELECT price, itemid, userid, bidid FROM bids where itemid=?");
  if(!$stmt){
  	printf("Query Prep Failed: %s\n", $mysqli->error);
  	exit;
  }
  $stmt->bind_param('s', $biditems[$j]->thisid);
  $stmt->execute();

  $stmt->bind_result($p, $ii,$bidderid, $bidid);

//find all bids attached to the items found above
  while($stmt->fetch()){

      $it = new bid(htmlentities($p), $ii, $bidderid, $bidid);
      array_push($output, $it);
  }

  $stmt->close();
}

//array output is full of bid ojects, right now containing price, itemid, and bidder id
//replace bidderid with username, replace itemid with item title
for($i = 0; $i < sizeof($output); $i++){
  $query = $mysqli->prepare("SELECT username FROM users WHERE id=?");
  if(!$query){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
  }
  $findme = $output[$i]->bidder;
  $query->bind_param('s', $findme);

  $query->execute();

  $query->bind_result($result);

  $query->fetch();
  $output[$i]->bidder = htmlentities($result);
  $query->close();

  $getitemitleq = $mysqli->prepare("SELECT title FROM items WHERE itemid=?");
  if(!$getitemitleq){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
  }
  $finditemtitle = $output[$i]->itemtitle;
  $getitemitleq->bind_param('s', $finditemtitle);

  $getitemitleq->execute();

  $getitemitleq->bind_result($r);

  $getitemitleq->fetch();
  $output[$i]->itemtitle = htmlentities($r);
  $getitemitleq->close();
}




echo json_encode(array(
  "success"=> true,
  "output" => $output
));

?>
